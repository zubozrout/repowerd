/*
 * Copyright © 2022 UBports Foundation.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authored by: Marius Gripsgard <marius@ubports.com>
 */

#include "real_device_info.h"

#include <deviceinfo.h>

repowerd::RealDeviceInfo::RealDeviceInfo() :
    deviceinfo(std::make_unique<::DeviceInfo>())
{}

std::string repowerd::RealDeviceInfo::name() {
    return deviceinfo->name();
}

bool repowerd::RealDeviceInfo::is_desktop() {
    return deviceinfo->deviceType() == ::DeviceInfo::Desktop;
}